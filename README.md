# Large Caps

This repository contains lists accompanying the article
**Exponentially Larger Affine and Projective Caps**
by [Christian Elsholtz](https://www.math.tugraz.at/~elsholtz/)
and [Gabriel F. Lipnik](https://www.gabriellipnik.at).